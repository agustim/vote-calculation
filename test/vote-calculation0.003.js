var chai = require('chai');
var expect = chai.expect;
var vc = require('../vote-calculation.js');

describe('Testing vote-calculation p-t-p:', function(){
  before(function(){
  })
  it('Calculate simple example', function(){
    var varTest = [ 1, 2, 0, 4, 1 ]
    expect(vc.toArray(vc.calculation(varTest))).to.deep.equal(varTest);
  })

  it('Calculate diffent example', function(){
    var varTest = [ 8, 33, 38, 23, 13, 10 ]
    expect(vc.toArray(vc.calculation(varTest))).to.deep.equal(varTest);

  })

  it('Calculate inverse simple', function(){
    var strTest = "\x00\x01\x03\x04"
    expect(vc.calculation(vc.toArray(strTest))).to.deep.equal(strTest);
  })


  it('Calculate inverse', function(){
    var strTest = "Vote for me!"
    expect(vc.calculation(vc.toArray(strTest))).to.deep.equal(strTest);
  })

})
