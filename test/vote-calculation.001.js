var expect = require('expect');
var vc = require('../vote-calculation.js');

describe('Testing create vote-calculation:', function(){
  before(function(){
  })
  it('Calculate Null', function(){
    expect(vc.calculation(null)).toBe("");
  })
  it('Calculate String', function(){
    expect(vc.calculation("test")).toBe("");
  })
  it('Calculate empty array', function(){
    expect(vc.calculation([])).toBe("");
  })
  it('Calculate simple example', function(){
    expect(vc.calculation([1,2,3,1,0])).toBe("12310");
  })
  it('Calculate example with diferents caracters', function(){
    expect(vc.calculation([26,11,16,1,13,10])).toBe("J;@1=:");
  })
})
