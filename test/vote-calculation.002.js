var chai = require('chai');
var expect = chai.expect;
var vc = require('../vote-calculation.js');

describe('Testing vote-calculation to array:', function(){
  before(function(){
  })
  it('Calculate Null', function(){
    expect(vc.toArray(null)).to.deep.equal([]);
  })
  it('Calculate Array', function(){
    expect(vc.toArray([0,1,2])).to.deep.equal([]);
  })
  it('Calculate empty string', function(){
    expect(vc.toArray("")).to.deep.equal([]);
  })
  it('Calculate simple example', function(){
    expect(vc.toArray("12310")).to.deep.equal([1,2,3,1,0]);
  })
  it('Calculate example with diferents caracters', function(){
    expect(vc.toArray("J;@1=:")).to.deep.equal([26,11,16,1,13,10]);
  })
})
