(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
      // AMD. Register as an anonymous module.
      define(['exports'], factory);
  } else if (typeof exports === 'object' && typeof exports.nodeName !== 'string') {
      // CommonJS
      factory(exports);
  } else {
      // Browser globals
      factory((root.voteCalculation = {}));
  }
}(this, function (exports) {

  exports.calculation = function(answers){
    if (Array.isArray(answers)) {
      return answers.map(function(n){
        n = (n % 78) + 48  // Caracter 48 fins al 126
        return String.fromCharCode(n)
      }).join('')
    } else {
      return ("")
    }
    return ("")
  }

  exports.toArray = function(str){
    if (typeof str === 'string' || str instanceof String) {
      return (str.split('').map(function(n){
          return (n.charCodeAt() - 48)
        })
      )
    } else {
      return([])
    }
  }
}));
